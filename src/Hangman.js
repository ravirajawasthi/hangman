import React, { Component } from 'react';
import uuid from 'uuid';
import { randomWord } from './words';
import './Hangman.css';
import img0 from './0.jpg';
import img1 from './1.jpg';
import img2 from './2.jpg';
import img3 from './3.jpg';
import img4 from './4.jpg';
import img5 from './5.jpg';
import img6 from './6.jpg';

class Hangman extends Component {
  /** by default, allow 6 guesses and use provided gallows images. */
  static defaultProps = {
    maxWrong: 6,
    images: [img0, img1, img2, img3, img4, img5, img6],
    gameOverMessage: (
      <h1 className="Hangman-gameOver">
        You loose{' '}
        <span role="img" aria-label="laughing">
          🤣
        </span>
      </h1>
    ),
    gameWinMessage: (
      <h1 className="Hangman-win">
        Great{' '}
        <span role="img" aria-label="Smug face">
          😁
        </span>
      </h1>
    )
  };

  constructor(props) {
    super(props);
    this.state = {
      nWrong: 0,
      guessed: new Set(),
      answer: randomWord(),
      gameOver: false,
      win: false
    };
    this.handleGuess = this.handleGuess.bind(this);
    this.restart = this.restart.bind(this);
  }

  /** guessedWord: show current-state of word:
    if guessed letters are {a,p,e}, show "app_e" for "apple"
  */
  guessedWord() {
    return this.state.answer
      .split('')
      .map(ltr => (this.state.guessed.has(ltr) ? ltr : '_'));
  }

  /** handleGuest: handle a guessed letter:
    - add to guessed letters
    - if not in answer, increase number-wrong guesses
  */

  correctGuess(newSet, answer) {
    for (let i = 0; i < answer.length; i += 1) {
      if (!newSet.has(answer[i])) {
        return false;
      }
    }
    return true;
  }

  handleGuess(evt) {
    let newState = {};
    let newSet = this.state.guessed;
    let val = evt.target.value;
    let valInAns = this.state.answer.includes(val);
    newSet.add(val);

    //for gameover and player looses the game
    if (this.state.nWrong + 1 === this.props.maxWrong && !valInAns) {
      newSet = new Set([...this.state.answer]);
      newState = {
        gameOver: true
      };
    }

    //guess maybe right or wrong, and there must me more than 1 chance left
    else {
      let win = this.correctGuess(newSet, this.state.answer);
      console.log(win);
      if (win) {
        newState = {
          win: win,
          gameOver: true
        };
      }
    }
    this.setState(st => {
      return {
        ...newState,
        guessed: newSet,
        nWrong: st.nWrong + (valInAns ? 0 : 1)
      };
    });
  }

  /** generateButtons: return array of letter buttons to render */
  generateButtons(gameOver) {
    console.log(this.state.answer);
    return 'abcdefghijklmnopqrstuvwxyz'.split('').map(ltr => (
      <button
        value={ltr}
        onClick={this.handleGuess}
        disabled={this.state.guessed.has(ltr)}
        key={uuid()}
      >
        {ltr}
      </button>
    ));
  }
  restart() {
    this.setState(st => {
      let newWord = randomWord();
      return {
        nWrong: 0,
        guessed: new Set(),
        answer: newWord,
        gameOver: false,
        win: false
      };
    });
  }
  /** render: render game */
  render() {
    const wrongGuess =
      this.state.nWrong === 0 ? (
        undefined
      ) : (
        <p className="Hangman-nWrong">
          Wrong guesses : {this.state.nWrong}/{this.props.maxWrong}
        </p>
      );
    return (
      <div className="Hangman">
        <h1>Hangman</h1>
        {this.state.gameOver ? (
          this.state.win ? (
            this.props.gameWinMessage
          ) : (
            this.props.gameOverMessage
          )
        ) : (
          <img
            alt={`${this.state.nWrong}/${this.props.maxWrong}`}
            src={this.props.images[this.state.nWrong]}
          />
        )}
        {wrongGuess}
        <p className="Hangman-word">{this.guessedWord()}</p>
        {this.state.gameOver ? (
          <button id="Hangman-restart" onClick={this.restart}>
            Restart
          </button>
        ) : (
          <p className="Hangman-btns">{this.generateButtons()}</p>
        )}
      </div>
    );
  }
}

export default Hangman;
